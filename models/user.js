const Joi = require('joi');
const config = require('config');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true,
    },
    lastNamePat:{
      type: String,
      required: true,
    },
    lastNameMat:{
      type: String
    },
    email: {
      type: String,
      required: true,
      unique: true,
      minlength: 5,
      maxlength: 255
    },
    password: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 1024
    },
    telephones: [{ 
      type: mongoose.Schema.Types.ObjectId, 
      ref: 'Telephone' 
    }],
    services:[{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Service'
    }],
    principalTelephone: {
      type: String,
      required: true,
      unique: true,
      minlength: 5,
      maxlength: 50
    },
    isActive: {
      type: Boolean,
      default: true
    }
  });

userSchema.methods.generateAuthToken = function() {
    const token = jwt.sign({ _id: this._id }, config.get('jwtPrivateKey'));
    return token;
}

const User = mongoose.model('User', userSchema);

function validateUser(user) {
  const schema = {
    name: Joi.string().required(),
    lastNamePat: Joi.string().required(),
    lastNameMat: Joi.string().optional(),
    email: Joi.string().min(5).max(255).required(),
    password: Joi.string().min(5).max(255).required(),
    telephone: Joi.string().min(5).max(50).required(),
    isActive: Joi.boolean().optional()
  };

  return Joi.validate(user, schema);
}

exports.User = User; 
exports.validate = validateUser;