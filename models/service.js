const Joi = require('joi');
const mongoose = require('mongoose');

const serviceSchema = new mongoose.Schema({
    name: {
      type: String,
      unique: true,
      required: true
    },
    description: {
      type: String,
      required: true,
      minlength: 5,
      maxlength: 1023
    },
    cost: {
      type: Number,
      required: true
    },
    users: [{ 
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User' 
    }],
    isActive: {
      type: Boolean,
      default: true
    }
  });

const Service = mongoose.model('Service', serviceSchema);

function validateService(service) {
  const schema = {
    name: Joi.string().required(),
    description: Joi.string().required().min(5).max(1023),
    cost: Joi.number().required()
  };

  return Joi.validate(service, schema);
}

exports.Service = Service; 
exports.validate = validateService;