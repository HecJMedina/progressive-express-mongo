const Joi = require('joi');
const mongoose = require('mongoose');

const Telephone = mongoose.model('Telephone',  new mongoose.Schema({
    telephone: {
      type: String,
      required: true,
      unique: true,
      minlength: 5,
      maxlength: 50
    },
    category: {
      type: String,
      required: true,
      enum: ['Principal','House','Cellphone','Business'],
      minlength: 5,
      maxlength: 15
    }
  }));

function validateTelephone(telephone) {
  const schema = {
    telephone: Joi.string().min(5).max(50).required(),
    category: Joi.string().min(5).max(255).required()
  };

  return Joi.validate(telephone, schema);
}

exports.Telephone = Telephone; 
exports.validate = validateTelephone;