const express = require('express');
const app = express();
const login = require('./api/login');
const users = require('./api/users');
const services = require('./api/services');
const telephones = require('./api/telephones');
const mongoose = require('mongoose');
const config = require('config');
const cors = require('cors');
const Fawn = require('fawn');

const port = process.env.PORT || 3100;
const server = app.listen(port, () => console.log(`Listening on port ${port}...`));

const db = config.get('db')
mongoose.connect(db).then(() => console.log(`Connected to ${db}...`)).catch(() => console.log('Could not connect to the database.'));

Fawn.init(mongoose);

if(!config.get('jwtPrivateKey')){
    throw new Error('FATAL ERROR: jwtPrivateKey is not defined');
}

app.use(express.json());
app.use(cors());
app.use('/api/login', login);
app.use('/api/users', users);
app.use('/api/services', services);
app.use('/api/telephones', telephones);


module.exports = server;