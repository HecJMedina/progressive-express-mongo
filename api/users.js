const bcrypt = require('bcrypt');
const {User, validate} = require('../models/user');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const _ = require('lodash');
const Joi = require('joi');
const {Telephone} = require('../models/telephone');
const {Service} = require('../models/service');
const mongoose = require('mongoose');
const Fawn = require('fawn');

//Fawn.init(mongoose);

router.get('/',auth, async (req,res) => {
  const user = await User.find().select('-password');
  res.send(user);
});

router.get('/:id',auth, async (req,res) => {
  const user = await User.findById(req.params.id).select('-password');
  if (!user) return res.status(404).send("100");
  
  res.send(user);
});


router.get('/telephones/:id',auth, async (req,res) => {
  const user = await User.find({ _id: req.params.id}).select('telephones').populate('telephones');
  if (!user) return res.status(404).send("100");

  res.send(user);
});

router.put('/:id',auth, async (req, res) => {
  const { error } = validatePut(req.body); 
  if (error) return res.status(400).send("101",{message:error.details[0].message});

  let user = await User.findById(req.params.id);
  if (!user) return res.status(404).send("100");

  user = {
    _id: user._id,
    name: req.body.name,
    lastNamePat: req.body.lastNamePat,
    lastNameMat: req.body.lastNameMat,
    email: req.body.email,
    password: user.password,
    telephones: user.telephones, 
    services: user.services,
    principalTelephone: req.body.telephone,
    isActive: req.body.isActive
  }

  try { 
    await new Fawn.Task()
    .update('users', { _id: mongoose.Types.ObjectId(req.params.id) }, user)
    .update('telephones', { _id: user.telephones[0] }, { telephone: req.body.telephone })
    .run();

    res.send(user);
  }
  catch (ex) {
    console.log(ex);
    return res.status(500).send("105");
  }
});

router.delete('/:id',auth, async (req, res) => {
  let user = await User.findById(req.params.id).select('-password');
  if (!user) return res.status(404).send("100");

  user.isActive = !user.isActive
  await User.update({ _id: req.params.id}, user);

  res.send(user);
});

router.post('/', auth, async (req, res) => {
  const { error } = validate(req.body); 
  if (error) return res.status(400).send("101",{message:error.details[0].message});

  let user = await User.findOne({ email: req.body.email });
  if(user) return res.status(400).send("102");

  let telephone = await Telephone.findOne({ telephone: req.body.telephone });
  if(telephone) return res.status(400).send("103");

  telephone = new Telephone({ telephone: req.body.telephone, category: "Principal"});

  user = new User({ 
    name: req.body.name, 
    lastNamePat: req.body.lastNamePat,
    lastNameMat: req.body.lastNameMat,
    email: req.body.email,  
    password: req.body.password,
    telephones: [ telephone._id ],
    principalTelephone: req.body.telephone,
    isActive: req.body.isActive
  });
  const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt);

  try {
    await new Fawn.Task()
    .save('users',user)
    .save('telephones',telephone)
    .run();
  }
  catch (ex) {
    console.log(ex);
    return res.status(500).send("105");
  }
  
  const token = user.generateAuthToken();

  res.header('x-auth-token',token).send(user);
});

router.post('/service/:id', auth, async (req, res) => {
  let service = await Service.findById(mongoose.Types.ObjectId(req.params.id));
  if (!service) return res.status(404).send("107");

  let user = await User.findById(req.user._id).select('services');
  if (!user) return res.status(404).send("100");

  if(user.services.includes(req.params.id) || service.users.includes(req.user._id))
    return res.status(400).send("108");

  try {
    await new Fawn.Task()
    .update('services', { _id: mongoose.Types.ObjectId(req.params.id) }, { $push: { users: mongoose.Types.ObjectId(req.user._id) } })
    .update('users', { _id: mongoose.Types.ObjectId(req.user._id) }, { $push: { services: mongoose.Types.ObjectId(req.params.id) } })
    .run();
  }
  catch (ex) {
    return res.status(500).send("105");
  }

  res.send({service: mongoose.Types.ObjectId(req.params.id), user: req.user._id})
});

router.get('/service/:id',auth, async (req,res) => {
  const user = await User.findById(req.params.id).select('services').populate('services');
  if (!user) return res.status(404).send("107");

  res.send(user);
});

router.delete('/service/:id',auth, async (req, res) => {
  let service = await Service.findById(req.params.id).select('users');
  if (!service) return res.status(404).send("107");

  let user = await User.findById(req.user._id).select('services');
  if (!user) return res.status(404).send("100");

  serviceIndex = user.services.indexOf(req.params.id);
  userIndex = service.users.indexOf(req.user._id);

  if(serviceIndex < 0 || userIndex < 0)
    return res.status(400).send("109");
    
  user.services.splice(serviceIndex,1);
  service.users.splice(userIndex,1);
  
  try {
    await new Fawn.Task()
    .update('services', { _id: mongoose.Types.ObjectId(req.params.id) }, { $set: { users: service.users } })
    .update('users', { _id: mongoose.Types.ObjectId(req.user._id) }, { $set: { services: user.services } })
    .run();
  }
  catch (ex) {
    return res.status(500).send("105");
  }

  res.send({user,service});
});

function validatePut(user) {
  const schema = {
    name: Joi.string().required(),
    lastNamePat: Joi.string().required(),
    lastNameMat: Joi.string().optional(),
    email: Joi.string().min(5).max(255).required(),
    telephone: Joi.string().min(5).max(50).required(),
    isActive: Joi.boolean().optional()
  };
  return Joi.validate(user, schema);
}

module.exports = router; 