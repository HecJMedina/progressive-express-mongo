const {User} = require('../models/user');
const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const {Telephone, validate} = require('../models/telephone');
const mongoose = require('mongoose');
const Fawn = require('fawn');

router.post('/:id', auth, async (req, res) => {
  const { error } = validate(req.body); 
  if (error) return res.status(400).send("101",{message:error.details[0].message});

  let telephone = await Telephone.findOne({ telephone: req.body.telephone });
  if(telephone) return res.status(400).send("103");

  telephone = new Telephone({ telephone: req.body.telephone, category: req.body.category });
  
  try {
    await new Fawn.Task()
    .save('telephones',telephone)
    .update('users', { _id: mongoose.Types.ObjectId(req.params.id) }, { $push: { telephones: telephone._id } })
    .run();
  }
  catch (ex) {
    console.log(ex);
    return res.status(500).send("105");
  }
  
  res.send({telephone, user_id: req.user._id});
});

module.exports = router; 