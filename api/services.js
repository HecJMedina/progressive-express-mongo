const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');
const Joi = require('joi');
const {Service, validate} = require('../models/service');

router.get('/',auth, async (req,res) => {
  const service = await Service.find();
  res.send(service);
});

router.get('/populate',auth, async (req,res) => {
    const service = await Service.find().populate({path:'users',select:'-password' });
    res.send(service);
});

router.get('/:id',auth, async (req,res) => {
  const service = await Service.findById(req.params.id).populate({path:'users',select:'-password' });
  if (!service) return res.status(404).send("107");
  
  res.send(service);
});

router.put('/:id',auth, async (req, res) => {
  const { error } = validate(req.body); 
  if (error) return res.status(400).send("101",{message:error.details[0].message});

  let service = await Service.findByIdAndUpdate(req.params.id,
    { 
        name: req.body.name,
        description: req.body.description,
        cost: req.body.cost
    }, { new: true })
    .catch(err => {return res.status(500).send("105")});
  if (!service) return res.status(404).send("107");

    res.send(service);
});

router.delete('/:id',auth, async (req, res) => {
  let service = await Service.findById(req.params.id);
  if (!service) return res.status(404).send("107");

  service = await Service.findByIdAndUpdate(req.params.id, {isActive: !service.isActive}, {new:true})
  .catch(err => {return res.status(500).send("105")});

  res.send(service);
});

router.post('/', auth, async (req, res) => {
  const { error } = validate(req.body); 
  if (error) return res.status(400).send("101",{message:error.details[0].message});

  let service = await Service.findOne({ name: req.body.name });
  if(service) return res.status(400).send("108");

  service = new Service({ 
    name: req.body.name,
    description: req.body.description,
    cost: req.body.cost
  });

  service = await service.save().catch( err => {return res.status(500).send("105")})

  res.send(service);
});

module.exports = router; 